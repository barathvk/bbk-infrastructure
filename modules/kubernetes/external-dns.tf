resource "helm_release" "external_dns" {
  repository = "https://charts.bitnami.com/bitnami"
  chart = "external-dns"
  name = "external-dns"
  namespace = "external-dns"
  create_namespace = true
  values = [
    <<EOF
    sources:
      - ingress
    provider: cloudflare
    cloudflare:
      email: ${var.cloudflare_email}
      apiKey: ${var.cloudflare_api_key}
      proxied: true
    domainFilters:
      - ${var.domain}
    txtOwnerId: ${var.name}
    logLevel: debug
    policy: sync
    rbac:
      create: true
    EOF
  ]
}