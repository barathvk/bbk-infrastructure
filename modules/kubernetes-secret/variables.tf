variable "name" {
  type = string
}
variable "data" {
  type = map(string)
}