variable "google_credentials" {}
variable "cloudflare_email" {}
variable "cloudflare_api_key" {}
variable "gitlab_email" {}
variable "gitlab_token" {}