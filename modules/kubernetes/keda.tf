resource "helm_release" "keda" {
  chart = "keda"
  name = "keda"
  namespace = "keda"
  create_namespace = true
  repository = "https://kedacore.github.io/charts"
  values = [
    <<EOF
    resources:
      requests:
        cpu: 10m
        memory: 10Mi
      limits:
        cpu: 100m
        memory: 250Mi
    EOF
  ]
}