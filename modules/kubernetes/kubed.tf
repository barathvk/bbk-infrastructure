resource "helm_release" "kubed" {
  chart = "kubed"
  name = "kubed"
  repository = "https://charts.appscode.com/stable"
  namespace = "kube-system"
}