module "database_secret" {
  count = var.database_username != "" ? 1 : 0
  source = "../../modules/kubernetes-secret"
  name = "db-credentials"
  data = {
    username = var.database_username
    password = var.database_password
  }
}