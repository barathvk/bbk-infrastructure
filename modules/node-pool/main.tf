resource "google_container_node_pool" "main" {
  name = var.name
  cluster = var.cluster_name
  location = var.google_location
  initial_node_count = var.min_node_count
  autoscaling {
    min_node_count = var.min_node_count
    max_node_count = var.max_node_count
  }
  management {
    auto_repair = true
    auto_upgrade = true
  }
  node_config {
    machine_type = var.node_machine_type
    preemptible = var.preemptible
    disk_size_gb = 100
    disk_type = "pd-ssd"
    service_account = var.service_account_email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}