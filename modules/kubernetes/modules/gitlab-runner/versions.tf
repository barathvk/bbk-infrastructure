terraform {
  required_version = ">= 1.0.6"
  required_providers {
    helm = {
      source = "hashicorp/helm"
    }
    google = {
      source = "hashicorp/google"
    }
  }
}