terraform {
  required_version = ">= 1.0.6"
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}