variable "name" {
  type = string
}
variable "google_project" {
  type = string
}
variable "domain" {
  type = string
}
variable "cloudflare_email" {
  type = string
}
variable "cloudflare_api_key" {
  type = string
}
variable "gitlab_email" {
  type = string
}
variable "gitlab_token" {
  type = string
}
variable "database_username" {
  type = string
  default = ""
}
variable "database_password" {
  type = string
  default = ""
}
variable "features" {
  type = object({
    gitlab_runner = bool,
  })
  default = {
    gitlab_runner = false,
  }
}