resource "helm_release" "loki_stack" {
  repository = "https://grafana.github.io/helm-charts"
  chart = "loki-stack"
  name = "loki"
  namespace = "monitoring"
  create_namespace = true
  values = [
    <<EOF
    grafana:
      enabled: false
    prometheus:
      enabled: true
      alertmanager:
        persistentVolume:
          enabled: false
      server:
        persistentVolume:
          enabled: false
    loki:
      persistence:
        enabled: true
        size: 5Gi
    EOF
  ]
}
resource "random_password" "grafana" {
  length = 16
  special = false
}
resource "helm_release" "grafana" {
  repository = "https://grafana.github.io/helm-charts"
  chart = "grafana"
  name = "grafana"
  namespace = "monitoring"
  create_namespace = true
  set {
    name = "adminPassword"
    value = random_password.grafana.result
  }
  values = [
    <<EOF
    adminPassword: ${random_password.grafana.result}
    grafana.ini:
      server:
        root_url: https://monitor.${var.domain}
    plugins:
      - marcusolsson-gantt-panel
    persistence:
      enabled: false
    datasources:
      datasources.yaml:
        apiVersion: 1
        datasources:
          - name: prometheus
            type: prometheus
            url: http://loki-prometheus-server
          - name: tempo
            type: tempo
            url: http://tempo:3100
          - name: loki
            type: loki
            url: http://loki:3100
    EOF
  ]
}

resource "helm_release" "tempo" {
  repository = "https://grafana.github.io/helm-charts"
  chart = "tempo"
  name = "tempo"
  namespace = "monitoring"
  create_namespace = true
}


resource "kubernetes_ingress" "grafana" {
  depends_on = [helm_release.grafana]
  metadata {
    name = "grafana"
    namespace = "monitoring"
    annotations = {
      "kubernetes.io/ingress.class" = "ambassador"
    }
  }
  spec {
    rule {
      host = "monitor.${var.domain}"
      http {
        path {
          path = "/"
          backend {
            service_name = "grafana"
            service_port = 80
          }
        }
      }
    }
  }
}