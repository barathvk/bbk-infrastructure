variable "name" {
  type = string
}
variable "google_location" {
  type = string
  description = "can be a google region or a google zone. if it is a region, one node will be created per zone in the region"
}
variable "kubernetes_version" {
  type = string
  default = "1.19.12"
}
variable "min_node_count" {
  type = number
  default = 3
}
variable "max_node_count" {
  type = number
  default = 8
}
variable "node_machine_type" {
  type = string
  default = "n1-standard-4"
}
variable "min_preemptible_node_count" {
  type = number
  default = 3
}
variable "max_preemptible_node_count" {
  type = number
  default = 8
}