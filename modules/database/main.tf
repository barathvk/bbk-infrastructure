resource "random_string" "database_unique" {
  length  = 4
  special = false
  upper   = false
}

resource "random_string" "database_password" {
  length  = 16
  special = false
}

resource "google_sql_database_instance" "database" {
  name             = "${var.name}-${random_string.database_unique.result}"
  database_version = "POSTGRES_9_6"

  region = var.google_region

  settings {
    tier              = "db-custom-1-3840"
    disk_autoresize   = true
    availability_type = var.availability_type

    backup_configuration {
      enabled    = true
      start_time = "00:00"
    }
  }
}

resource "google_sql_user" "database_user" {
  name     = "${var.name}-db-user"
  instance = google_sql_database_instance.database.name
  password = random_string.database_password.result
}

output "username" {
  value = google_sql_user.database_user.name
}
output "password" {
  value = google_sql_user.database_user.password
}