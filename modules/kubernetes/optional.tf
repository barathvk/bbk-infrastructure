module "gitlab_runner" {
  count = var.features.gitlab_runner ? 1 : 0
  source = "./modules/gitlab-runner"
  gitlab_host = "git.brickblock.sh"
  name = var.name
  runner_registration_token = "-L_MVtEbE8nHXMyAy2Ue"
  runner_concurrent = 10
  gcloud_auth_secret_name = module.gcloud_auth_secret.secret_name
}