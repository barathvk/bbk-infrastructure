variable "name" {
  type = string
}
variable "google_location" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "node_machine_type" {
  type = string
}
variable "min_node_count" {
  type = number
}
variable "max_node_count" {
  type = number
}
variable "service_account_email" {
  type = string
}
variable "preemptible" {
  type = bool
  default = false
}