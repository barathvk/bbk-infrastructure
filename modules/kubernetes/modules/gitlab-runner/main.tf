resource "helm_release" "gitlab_runner" {
  repository = "https://charts.gitlab.io"
  chart = "gitlab-runner"
  name = "gitlab-runner"
  namespace = "gitlab"
  create_namespace = true
  values = [
    <<EOF
      gitlabUrl: https://${var.gitlab_host}/
      runnerRegistrationToken: ${var.runner_registration_token}
      envVars:
        - name: FF_GITLAB_REGISTRY_HELPER_IMAGE
          value: 1
      certsSecretName: ${module.certificate_secret.secret_name}
      concurrent: ${var.runner_concurrent}
      checkInterval: ${var.check_interval}
      rbac:
        create: true
        clusterWideAccess: false
      runners:
        image: ubuntu:20.04
        privileged: true
        builds:
          cpuRequests: 100m
          memoryRequests: 128Mi
        services:
          cpuRequests: 100m
          memoryRequests: 128Mi
        helpers:
          cpuRequests: 100m
          memoryRequests: 128Mi
        cache:
          cacheType: gcs
          gcsBucketName: ${google_storage_bucket.main.name}
          secretName: ${var.gcloud_auth_secret_name}
    EOF
  ]
}