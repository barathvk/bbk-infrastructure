resource "tls_private_key" "main" {
  algorithm = "ECDSA"
}
resource "tls_self_signed_cert" "main" {
  key_algorithm         = tls_private_key.main.algorithm
  private_key_pem       = tls_private_key.main.private_key_pem
  validity_period_hours = 8760 // 1 Year
  allowed_uses = [
    "cert_signing",
  ]

  dns_names = [var.gitlab_host]

  subject {
    common_name  = var.gitlab_host
    organization = "Brickblock"
  }
}
module "certificate_secret" {
  source = "../../../kubernetes-secret"
  name = "gitlab-domain-cert"
  data = {
    gitlab-domain-cert = tls_self_signed_cert.main.cert_pem
  }
}