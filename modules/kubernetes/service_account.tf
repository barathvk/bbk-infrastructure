locals {
  roles = [
    "roles/storage.objectCreator", // Gives Kubernetes the permission to write objects into storage buckets
    "roles/storage.objectViewer", // Gives Kubernetes the permission to read objects from storage buckets
    "roles/cloudsql.editor", // Gives Kubernetes the permission to interact with the created database instance
    "roles/cloudkms.cryptoKeyEncrypterDecrypter", // Gives Kubernetes the permission to encrypt and decrypt using KMS keys
    "roles/container.developer", // Gives GitLab CI the permission to make changes on the Kubernetes cluster i.e. deploy apps
    "roles/storage.admin", // Gives GitLab the permission to administer objects in storage buckets
  ]
}
resource "google_project_iam_custom_role" "role_pubsub" {
  role_id     = "pubsub"
  title       = "PubSub consumer and producer"
  description = "Role that includes permissions to subscribe and publish to topics as well as creating subscriptions"
  permissions = ["pubsub.topics.publish", "pubsub.subscriptions.consume", "pubsub.topics.attachSubscription", "pubsub.subscriptions.get", "pubsub.subscriptions.list", "pubsub.topics.get", "pubsub.topics.list", "pubsub.subscriptions.create"]
}
resource "google_service_account" "main" {
  account_id = var.name
  display_name = var.name
}
resource "google_service_account_key" "main" {
  service_account_id = google_service_account.main.name
}
module "gcloud_auth_secret" {
  source = "../../modules/kubernetes-secret"
  name = "gcloud-auth"
  data = {
    "credentials.json" = base64decode(google_service_account_key.main.private_key)
    "gcs-application-credentials-file" = base64decode(google_service_account_key.main.private_key)
  }
}
resource "google_project_iam_member" "kubernetes_pubsub" {
  depends_on = [google_project_iam_custom_role.role_pubsub]
  role       = "projects/${var.google_project}/roles/${google_project_iam_custom_role.role_pubsub.role_id}"
  member     = "serviceAccount:${google_service_account.main.email}"
}
resource "google_project_iam_member" "main" {
  for_each = toset(local.roles)
  member = "serviceAccount:${google_service_account.main.email}"
  role = each.key
}