terraform {
  required_version = ">= 1.0.6"
  backend "remote" {
    organization = "brickblock"
    workspaces {
      name = "development"
    }
  }
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.84.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.5.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.3.0"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.26.1"
    }
  }
}

provider "google" {
  project = local.google_project
  region = local.google_region
  credentials = var.google_credentials
  zone = local.google_zone
}
provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}