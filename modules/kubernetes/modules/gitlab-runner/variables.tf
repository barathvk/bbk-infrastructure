variable "name" {
  type = string
}
variable "gitlab_host" {
  type = string
}
variable "runner_registration_token" {
  type = string
}
variable "runner_concurrent" {
  type = number
  default = 10
}
variable "check_interval" {
  type = number
  default = 30
}
variable "gcloud_auth_secret_name" {
  type = string
}