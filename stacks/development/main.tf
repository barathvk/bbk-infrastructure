locals {
  google_project = "bvk-development"
  google_region = "europe-west3"
  google_zone = "${local.google_region}-b"
  name = "development"
  domain = "barathvk.dev"
  //noinspection HILUnresolvedReference
  cluster_kube_config = module.cluster.cluster.master_auth[0]
}
data "google_client_config" "current" {}
module "domain" {
  source = "../../modules/domain"
  domain = local.domain
}
module "cluster" {
  source = "../../modules/cluster"
  google_location = local.google_zone // no high availability
  name = local.name
  min_node_count = 1
  max_node_count = 2
  min_preemptible_node_count = 0
}

module "database" {
  source = "../../modules/database"
  google_region = local.google_region
  name = local.name
}

//noinspection HILUnresolvedReference
provider kubernetes {
  alias = "cluster_kubernetes"
  host                   = "https://${module.cluster.cluster.endpoint}"
  token                  = data.google_client_config.current.access_token
  cluster_ca_certificate = base64decode(local.cluster_kube_config.cluster_ca_certificate)
}

provider helm {
  alias = "cluster_helm"
  //noinspection HILUnresolvedReference
  kubernetes {
    host                   = "https://${module.cluster.cluster.endpoint}"
    token                  = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(local.cluster_kube_config.cluster_ca_certificate)
  }
}

module "kubernetes" {
  depends_on = [module.cluster]
  source = "../../modules/kubernetes"
  domain = local.domain
  google_project = local.google_project
  name = local.name
  cloudflare_email = var.cloudflare_email
  cloudflare_api_key = var.cloudflare_api_key
  gitlab_email = var.gitlab_email
  gitlab_token = var.gitlab_token
  database_username = module.database.username
  database_password = module.database.password
  providers = {
    kubernetes = kubernetes.cluster_kubernetes
    helm = helm.cluster_helm
  }
}