resource "helm_release" "ambassador" {
  repository = "https://www.getambassador.io"
  chart = "ambassador"
  name = "ambassador"
  namespace = "ambassador"
  create_namespace = true
  values = [
    <<EOF
      enableAES: false
    EOF
  ]
}