variable "name" {
  type = string
}
variable "google_region" {
  type = string
}
variable "availability_type" {
  type = string
  default = "ZONAL"
}