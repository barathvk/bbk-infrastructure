locals {
  auth_string = base64encode("${var.gitlab_email}:${var.gitlab_token}")
}
module "container_registry" {
  source = "../../modules/kubernetes-secret"
  name = "brickblock-registry"
  data = {
    ".dockerconfigjson" = <<EOF
    {
      "auths": {
        "username": "${var.gitlab_email}",
        "password": "${var.gitlab_token}",
        "email": "${var.gitlab_email}",
        "auth": "${local.auth_string}"
      }
    }
    EOF
  }
}