resource "google_service_account" "main" {
  account_id = "bbk-${var.name}-cluster"
  display_name = "bbk-${var.name}-cluster"
}
resource "google_container_cluster" "main" {
  name = var.name
  location = var.google_location
  initial_node_count = 1
  remove_default_node_pool = true
  min_master_version = var.kubernetes_version
  addons_config {
    http_load_balancing {
      disabled = true
    }
  }
}

module "default_node_pool" {
  depends_on = [google_container_cluster.main]
  source = "../node-pool"
  cluster_name = google_container_cluster.main.name
  google_location = var.google_location
  min_node_count = var.min_node_count
  max_node_count = var.max_node_count
  name = "${var.name}-default-node-pool"
  node_machine_type = var.node_machine_type
  service_account_email = google_service_account.main.email
  preemptible = false
}

module "preemptible_node_pool" {
  depends_on = [google_container_cluster.main]
  count = var.min_preemptible_node_count > 0 ? 1 : 0
  source = "../node-pool"
  cluster_name = google_container_cluster.main.name
  google_location = var.google_location
  min_node_count = var.min_preemptible_node_count
  max_node_count = var.max_preemptible_node_count
  name = "${var.name}-preemptible-node-pool"
  node_machine_type = var.node_machine_type
  service_account_email = google_service_account.main.email
  preemptible = true
}

output "cluster" {
  value = google_container_cluster.main
}